<?php
session_start();
require_once("../Modelo/M_Tramite.php");
$objTramite = new M_Tramite();


function quitar_tildes($cadena)
{
	$no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ", "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹");
	$permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
	$texto = str_replace($no_permitidas, $permitidas, $cadena);
	return $texto;
}


switch ($_POST["opcion"]) {
	case 1:
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			//obtenemos el archivo a subir
			$path = pathinfo($_FILES['archivo_entidad']['name']);

			//obtenemos el nombre del archivo
			//$file_nom = utf8_decode($path['filename']);


			$file_nombre = quitar_tildes($path['filename']);

			//echo $file_nom;

			//obtenemos la extension del archivo
			$file_ext = $path['extension'];

			//asignar un identificador para evitar que un archivo se repita
			$var = date("YmdHis");

			//modificamos el nombre para evitar que se repita
			$nuevo_nombre = $file_nombre . "_" . $var . "." . $file_ext;

			//comprobamos si existe un directorio para subir el archivo
			//si no es así, lo creamos
			if (!is_dir("../Vista/adjuntos/"))
				mkdir("../Vista/adjuntos/", 0777);

			//comprobamos si el archivo ha subido
			//if ($file && move_uploaded_file($_FILES['imagen'.$idRequisito]['tmp_name'],"../Vista/adjuntos/".$nuevo_nombre)){
			if (move_uploaded_file($_FILES['archivo_entidad']['tmp_name'], "../Vista/adjuntos/" . $nuevo_nombre)) {
				//insertar el nombre del archivo
				$asunto = addslashes(strtoupper(trim($_POST["asunto"])));
				$entidad = addslashes(strtoupper(trim($_POST["entidad"])));
				$representante = addslashes(strtoupper(trim($_POST["representante"])));
				$correo = trim($_POST["correo"]);
				$sumilla = addslashes(strtoupper(trim($_POST["sumilla"])));

				$objTramite->registrar_tramite($_POST["tipo_tramite"], $asunto, $_POST["tipo_persona"], $entidad, $representante, $_POST["tipo_documento"], $_POST["nrodocumento"], $correo, $_POST["telefono"], $sumilla, $nuevo_nombre);
				sleep(1); //retrasamos la petición 1 segundo

			} else {
				throw new Exception("El archivo no se ha direccionado", 1);
			}
		} else {
			throw new Exception("Error al procesar", 1);
		}
		break;

	case 2:
		$objTramite->mostrar_listado();
		break;

	case 3:
		$objTramite->mostrar_evaluar($_POST["idTramite"]);
		break;

	case 4:
		$objTramite->mostrar_rechazar_tramite($_POST["idTramite"]);
		break;

	case 5:
		$observaciones = strtoupper(trim($_POST["observaciones"]));
		$objTramite->guardar_rechazo_tramite($_POST["idTramite"], $observaciones);
		break;

	case 6:
		$objTramite->mostrar_aprobar_tramite($_POST["idTramite"]);
		break;

	case 7:
		$objTramite->mostrar_responsable($_POST["oficina_destino"]);
		break;

	case 8:
		$mensaje = strtoupper(trim($_POST["mensaje"]));
		$objTramite->guardar_respuesta($_POST["idTramite"], $_POST["oficina_destino"], $mensaje);
		break;

	case 9:
		$objTramite->mostrar_detalles($_POST["idTramite"]);
		break;
}
