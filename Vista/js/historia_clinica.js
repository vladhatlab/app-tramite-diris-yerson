// JavaScript Document
$(document).ready(function() {
	$('#historias_clinicas').DataTable();
	$('[data-toggle="tooltip"]').tooltip();
	
	$('#tipo_documento').change(function(){
		if($(this).val() == 9){//Si es SIN DOCUMENTO
			$("#nrodocumento").prop('disabled',true);
		}else{
			$("#nrodocumento").prop('disabled',false);
		}
	});
	$("#nrodocumento").prop("disabled", true);
	//validar cantidad de digitos de numero de documento
	$("#tipo_documento").change(function () {
		$("#tipo_documento option:selected").each(function() {
			tipo=$(this).val();
			if(tipo==2){
				$("#nrodocumento").prop('disabled',false);
				$("#nrodocumento").attr("maxlength", 8);
			}else if(tipo==3){
				$("#nrodocumento").prop('disabled',false);
				$("#nrodocumento").attr("maxlength", 12);
			}else{
				$("#nrodocumento").prop('disabled',true);
				$('#nrodocumento').val('');
			}
		});
	});
	
	$("#fecha_nac").keyup(function () {
		cadena=$(this).val();
		subcadena=cadena.split("/");
		if(cadena.length==2 && cadena.indexOf('/')==-1){
			$("#fecha_nac").val(cadena + "/");
		}else if(cadena.length==5 && subcadena.length==2 && subcadena.indexOf('/')==-1){
			$("#fecha_nac").val(cadena + "/");
		}
	});
	
	$('#nrodocumento, #tel_fijo, #tel_celular, #tel_fijoe, #tel_celulare, #nrohistoria').numeric();
	
	$("#departamento").change(function () {
		$("#departamento option:selected").each(function () {
	    	departamento=$(this).val();
	        $.post("../Controlador/C_Historia.php", { departamento: departamento, opcion: "1" }, function(data){
	        	$("#provincia").html(data);
	      	});     
	    });
	});
	
	$("#provincia").change(function () {
		$("#provincia option:selected").each(function () {
	    	departamento=$("#departamento option:selected").val();
			provincia=$(this).val();
	        $.post("../Controlador/C_Historia.php", { departamento: departamento, provincia: provincia, opcion: "2" }, function(data){
	        	$("#distrito").html(data);
	      	});     
	    });
	});
	$("#distrito").change(function () {
		$("#distrito option:selected").each(function () {
	    	departamento=$("#departamento option:selected").val();
			provincia=$("#provincia option:selected").val();
			distrito=$(this).val();
	        $.post("../Controlador/C_Historia.php", { departamento: departamento, provincia: provincia, distrito: distrito, opcion: "3" }, function(data){
	        	$("#localidad").html(data);
	      	});     
	    });
	});
	
	$("#localidad").change(function () {
		$("#localidad option:selected").each(function () {
			localidad=$(this).val();
	        $.post("../Controlador/C_Historia.php", { localidad: localidad, opcion: "4" }, function(data){
	        	$("#geo").html(data);
	      	});     
	    });
	});
	
	$("#localidad2").change(function () {
		$("#localidad2 option:selected").each(function () {
			localidad=$(this).val();
	        $.post("../Controlador/C_Historia.php", { localidad: localidad, opcion: "4" }, function(data){
	        	$("#geo").html(data);
	      	});     
	    });
	});
	
	$("#seguro").change(function () {
		$("#seguro option:selected").each(function () {
			seguro=$(this).val();
	        $.post("../Controlador/C_Historia.php", { seguro: seguro, opcion: "5" }, function(data){
	        	$("#tipo_sis_2").html(data);
	      	});     
	    });
	});
	
	$("#tipo_sis").change(function () {
		$("#tipo_sis option:selected").each(function () {
			tipo_sis=$(this).val();
	        $.post("../Controlador/C_Historia.php", { tipo_sis: tipo_sis, opcion: "6" }, function(data){
	        	$("#tipo_sis_33").html(data);
	      	});     
	    });
	});
	
	$("#fecha_nac").change(function () {
		fecha_nac=$(this).val();
		$.post("../Controlador/C_Historia.php", { fecha_nac: fecha_nac, opcion: "7" }, function(data){
			$("#edad").html(data);
		});
	});

	$('#nueva_historia').on("shown.bs.tab", function (e) {
		google.maps.event.trigger(map, 'resize');

		var map = new google.maps.Map(document.getElementById('map'),
		{
			zoom: 16,
			center:new google.maps.LatLng(-12.00744209,-77.07553704),
		});
		
		var marker = new google.maps.Marker({
			map: map,
			draggable: true,
			animation: google.maps.Animation.DROP,
			position: new google.maps.LatLng(-12.00744209,-77.07553704),
		});
		
		marker.addListener('click', toggleBounce);
		  
		marker.addListener('dragend', function (event){
			document.getElementById('coords').value = this.getPosition().lat()+','+ this.getPosition().lng();
		});
	
		function toggleBounce(){
			if (marker.getAnimation() !== null) {
				marker.setAnimation(null);
			}else{
				marker.setAnimation(google.maps.Animation.BOUNCE);
			}
		}
	});
	
	$("#localizacion").hide();
	$("#localizacion2").hide();
	$("#tipo_sis_3").hide();
	
	$("#formulario-historia").submit(function(event){
		var dataString = $("#formulario-historia").serialize();
		$.ajax({
			type: "POST",
			url: "../Controlador/C_Historia.php",
			data: dataString,
			beforeSend: function(){
				$('#nueva_historia').modal("hide");
				$('#barra').modal("show");
			},
			success: function(data){
				$('#barra').modal("hide");
				$('#reg_exito').modal("show");
				$('#mensaje_respuesta').html(data);
			}
		})
		event.preventDefault()
	});
	
	$("#btnedit").click(function(event){
		//alert('sdfs');
		var dataString = $("#formulario-historia-edicion").serialize();
		$.ajax({
			type: 'POST',
			url: '../Controlador/C_Historia.php',
			data: dataString,
			beforeSend: function(){
				$('#editar_historia').modal('hide');
				$('#barra').modal('show');
			},
			success: function(data){
				$('#barra').modal('hide');
				$('#reg_exito').modal('show');
				$('#mensaje_respuesta').html(data);
			}
		})
		event.preventDefault()
	})
});

function cargar_edicion(nrodocumento){
	$('#editar_historia').modal("show");
	$.ajax({
		url: "../Controlador/C_Historia.php",
		type: "POST",
		data: 'opcion=9&nrodocumento='+nrodocumento,
		success: function(data){
			$("#editar_historia").html(data);
		}
	});
}

function historial_historia(nrodocumento){
	$('#historial_historia').modal("show");
	$.ajax({
		url: "../Controlador/C_Historia.php",
		type: "POST",
		data: 'opcion=11&nrodocumento='+nrodocumento,
		success: function(data){
			$("#historial_historia").html(data);
		}
	});
}

