<?php include("includes/_cabecera.php")?>
<script language="javascript">
    $(document).ready(function(){
        $("#tipo_tramite").select2({
            width:'100%'
        });

        $('#nrodocumento, #telefono').numeric();

        $("#asunto, #nrodocumento").prop("disabled", true);
        //validar cantidad de digitos de numero de documento
        $("#tipo_tramite").change(function () {
            $("#tipo_tramite option:selected").each(function() {
                tipo=$(this).val();
                if(tipo!=""){
                    $("#asunto").prop('required',true);
                    $("#asunto").prop('disabled',false);
                }else{
                    $("#asunto").prop('required',true);
                    $("#asunto").prop('disabled',true);
                    $('#asunto').val('');
                }
            });
        });

        $("#tipo_documento").change(function () {
            $("#tipo_documento option:selected").each(function() {
                tipo=$(this).val();
                if(tipo=="01"){
                    $("#nrodocumento").prop('disabled',false);
                    $("#nrodocumento").attr("maxlength", 8);
                }else if(tipo=="02" || tipo=="99"){
                    $("#nrodocumento").prop('disabled',false);
                    $("#nrodocumento").attr("maxlength", 15);
                }else{
                    $("#nrodocumento").prop('required',true);
                    $("#nrodocumento").prop('disabled',true);
                    $('#nrodocumento').val('');
                }
            });
        });

        $("#formulario-tramite").submit(function(event){
            var dataString = new FormData($("#formulario-tramite")[0]);
            $.ajax({
                type: "POST",
                url: "../Controlador/C_Tramite.php",
                data: dataString,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $('#registrar').html("Registrando...").prop('disabled',true);
                    $('#barra').modal("show");
                },
                success: function(data){
                    $('#barra').modal("hide");
                    $('#reg_exito').modal("show");
                    $('#reg_exito').html(data);
                }
            })
            event.preventDefault()
        });

        $(".close").click(function(){
            $("#formulario-tramite")[0].reset();
        });

        $("#nom_repre").html('<b>Nombres y Apellidos:</b>');

        $("#enti").hide('fast');
        $("input:radio[name=tipo_persona]").click(function(){
            var cit = $("input:radio[name=tipo_persona]:checked").val();
            if(cit == "" || cit == "1"){
                $("#enti").hide('fast');
                $("#nom_repre").html('<b>Nombres y Apellidos:</b>');
                $("#entidad").prop('required',false);
            }else if(cit == "2"){
                $("#enti").show('fast');
                $("#nom_repre").html('<b>Representante:</b>');
                $("#entidad").prop('required',true);
            }
        });

        //queremos que esta variable sea global
        var fileExtension = "";
        //función que observa los cambios del campo file y obtiene información
        $(':file').click(function(){
            $('#archivo_entidad').change(function(){ 
                //obtenemos un array con los datos del archivo
                var file = $('#archivo_entidad')[0].files[0];
                //obtenemos el nombre del archivo
                var fileName = file.name;
                //obtenemos la extensión del archivo
                var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
                
                if(fileExtension == "pdf" || fileExtension == "PDF"){
                    //habilitar el boton
                    validar_boton(2);
                    //obtenemos el tamaño del archivo
                    var fileSize = file.size;
                    //obtenemos el tipo de archivo image/png ejemplo
                    var fileType = file.type;
                    
                    //mensaje con la información del archivo
                    showMessage("<span class='alert alert-success'><b>Archivo aprobado</b></span>");
                }else{
                    //deshabilitar el boton
                    validar_boton(1);
                    //mensaje con la información del archivo
                    showMessage("<span class='alert alert-danger'><b>El archivo no es un PDF</b></span>");
                }
            });
        });
        
        function validar_boton(valor){
            if(valor == 1){
                $('#registrar').prop('disabled',true);
            }else if(valor == 2){
                $('#registrar').prop('disabled',false);
            }
        }
        
        function showMessage(message){
            //$("#respuesta_carga").html("").show();
            $("#mensaje").html(message);
        }
    });
</script>
<style>
    .separar{margin:20px;}
    .separar2{margin-top:45px; margin-bottom:30px;}
    .aprobar{font-size:18px; color:#009966; font-weight:bold;}
    .rechazar{font-size:18px; color:#FF0000; font-weight:bold;}
    .container{width:70%;}
</style>
</head>

<body style="background-color: #F8F8F8;">
<nav class="nav navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <a class="navbar-brand" href="main.php" style="padding-top:0px;"><img src="images/logo_diris_sur.png" width="300" style="height: 50px;" class="img-responsive"></a>
    </div>
    <ul class="nav navbar-nav navbar-right">
        <a class="btn btn-success" href="http://consultatramite.dirislimasur.gob.pe:8085/consulta/" target="_blank">CONSULTA TU<BR />EXPEDIENTE</a>
    </ul>
</nav>

<div class="container" style="padding-top:60px;">
    <div class="panel panel-info">
    	<div class="panel-heading"><strong>REGISTRO DE TRÁMITE VIRTUAL</strong></div>
        <form id="formulario-tramite" name="formulario-tramite" autocomplete="off" enctype="multipart/form-data">
    	<div class="panel-body">
            <input type="hidden" name="opcion" value="1">
            <div class="tab-content">
                <div class="row separar">
                    <div class="col-md-2 text-right"><b>Tipo Trámite:</b></div>
                    <div class="col-md-4">
                        <select class="form-control" id="tipo_tramite" name="tipo_tramite" required>
                            <option value="">Seleccione:</option>
                            <?php
                            $sql2="SELECT cDocuType, sDocuDesc FROM Tipo_Documento WHERE cNumbFlag = '0' ORDER BY sDocuDesc ASC";
                            $Tra=$datos->listar($sql2);
                            foreach($Tra as $tra):
                            ?>
                            <option value="<?=$tra["cDocuType"]?>"><?=strtoupper(utf8_encode($tra["sDocuDesc"]));?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-md-1 text-right"><b>Asunto:</b></div>
                    <div class="col-md-5"><input type="text" class="form-control text-uppercase" id="asunto" name="asunto"></div>
                </div>
                <h4>Datos del Remitente</h4>
                <div class="row separar">
                    <div class="col-md-2 text-center"></div>
                    <div class="col-md-4 text-center"><label class="radio-inline aprobar"><input type="radio" name="tipo_persona" id="tipo_persona" value="1" checked>PERSONA NATURAL</label></div>
                    <div class="col-md-5 text-center"><label class="radio-inline rechazar"><input type="radio" name="tipo_persona" id="tipo_persona" value="2">ENTIDAD PÚBLICA, PRIVADA U ONG</label></div>
                    <div class="col-md-1 text-center"></div>
                </div>
                <div class="row separar" id="enti">
                    <div class="col-md-2 text-right"><b>Nombre Entidad:</b></div>
                    <div class="col-md-9"><input type="text" class="form-control text-uppercase" id="entidad" name="entidad"></div>
                </div>
                <div class="row separar">
                    <div class="col-md-2 text-right" id="nom_repre"><b>Representante:</b></div>
                    <div class="col-md-8"><input type="text" class="form-control text-uppercase" id="representante" name="representante" required></div>
                </div>
                <div class="row separar">
                    <div class="col-md-2 text-right"><b>Tipo Documento:</b></div>
                    <div class="col-md-3">
                        <select class="form-control" id="tipo_documento" name="tipo_documento" required>
                            <option value="">Seleccione:</option>
                            <?php
                            $sql3="SELECT TipDocPerCCod, TipDocPerTDescrip FROM oeimTipDocPersonal ORDER BY TipDocPerCCod ASC";
                            $Doc=$datos->listar($sql3);
                            foreach($Doc as $doc):
                            ?>
                            <option value="<?=$doc["TipDocPerCCod"]?>"><?=strtoupper(utf8_encode($doc["TipDocPerTDescrip"]));?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-md-2 text-right"><b>N° Documento:</b></div>
                    <div class="col-md-3"><input type="text" class="form-control text-uppercase" id="nrodocumento" name="nrodocumento" required></div>
                </div>
                <div class="row separar">
                    <div class="col-md-2 text-right"><b>Correo:</b></div>
                    <div class="col-md-4"><input type="text" class="form-control" id="correo" name="correo" required></div>
                    <div class="col-md-1 text-right"><b>Teléfono:</b></div>
                    <div class="col-md-3"><input type="text" class="form-control text-uppercase" id="telefono" name="telefono" maxlength="9" required></div>
                    <div class="col-md-2 text-right"></div>
                </div>
                <div class="row separar" id="enti">
                    <div class="col-md-2 text-right"><b>Sumilla:</b></div>
                    <div class="col-md-9"><textarea name="sumilla" id="sumilla" cols="85%" rows="4" style="text-transform:uppercase;"></textarea></div>
                </div>
                <h4>Adjuntar Archivo <span class="label label-warning">SOLO PDF</span></h4>
                <div class="row separar">
                    <div class="col-md-2 text-right"><b>Archivo:</b></div>
                    <div class="col-md-6"><input type="file" id="archivo_entidad" name="archivo_entidad" required></div>
                    <div class="col-md-4"><div id="mensaje"></div></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="text-center">
                <button class="btn btn-primary" type="submit" id="registrar">Registrar Trámite</button>
            </div>
        </div>
        </form>
    </div>
</div>

<div id="reg_exito" class="modal fade" role="dialog" data-backdrop="static" style="padding-top:15%;"></div>

<div id="barra" class="modal fade" role="dialog" data-backdrop="static" style="padding-top:15%;">
	<div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <h4 class="modal-title">Registrando</h4>
            </div>
            <div class="modal-body">
            	<div class="tab-content">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include("includes/_footer.php")?>
