<br />
<footer class="footer">
	<div class="container">
    	<div class="col-md-6"><h5><small>Dirección de Redes Integradas de Salud Lima Sur</small></h5></div>
        <div class="col-md-6 text-right"><h5><small>&copy; Todos los Derechos Reservados 2020</small></h5></div>
    </div>
</footer>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
</body>
</html>