<?php
session_start();
if(empty($_SESSION["idUsuario"])){
    header("Location: ../index_login.php");
}
require_once("../Modelo/M_Datos.php");
$datos=new M_Datos();
?>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="css/estilos.css" rel="stylesheet" media="screen">
<link href="css/select2.min.css" rel="stylesheet" />
<link href="css/bootstrap-datetimepicker.css" rel="stylesheet" />
<title>..::MESA DE PARTES VIRTUAL - DIRIS LIMA SUR::..</title>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="js/buttons.flash.min.js"></script>
<script type="text/javascript" src="js/jszip.min.js"></script>
<script type="text/javascript" src="js/buttons.html5.min.js"></script>
<script type="text/javascript" src="js/buttons.print.min.js"></script>-->
<script src="js/select2.min.js"></script>
<script type="text/javascript" src="js/jquery.numeric.js"></script>
<script type="text/javascript" src="js/moments.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
<script language="javascript">
    $(document).ready(function(){
		$("#formulario-contrasena").submit(function(event){
            var dataString = $("#formulario-contrasena").serialize();
            $.ajax({
                type: "POST",
                url: "../Controlador/C_Usuarios.php",
                data: dataString,
                beforeSend: function(){
                    $('#actualizar').prop('disabled',true);
                    $('#editar_contrasena').modal("hide");
                    $('#barra').modal("show");
                },
                success: function(data){
                    $('#barra').modal("hide");
                    $('#actualizar').prop('disabled',false);
                    $('#contra').html(data);
                }
            })
            event.preventDefault();
        });

		$(".close").click(function(){
            $("#formulario-contrasena")[0].reset();
        });
	});
</script>
