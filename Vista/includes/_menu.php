<body style="background-color: #F8F8F8;">
<nav class="nav navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <a class="navbar-brand" href="main.php" style="padding-top:0px;"><img src="images/logo_diris_sur.png" width="300" style="height: 50px;" class="img-responsive"></a>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Listados<span class="glyphicon glyphicon-list-alt"></span><span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="lista_tramite.php">Pendientes</a></li>
                    <li><a href="lista2.php">Todos</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <?php
            $usu="SELECT nombres FROM usuarios WHERE idUsuario='$_SESSION[idUsuario]'";
            $Rs3=$datos->listar($usu);
            ?>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <?=utf8_encode($Rs3[0]["nombres"]);?><b class="caret"></b><?echo "&nbsp;&nbsp;&nbsp;";?></a>
                <ul class="dropdown-menu">
                <li>
                    <a href="" data-toggle="modal" data-target="#editar_contrasena"><span class="glyphicon glyphicon-pencil"></span> Cambiar Contraseña&nbsp;&nbsp;&nbsp;</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="../Modelo/logout.php"><span class="glyphicon glyphicon-user"></span> Cerrar Sesión&nbsp;&nbsp;&nbsp;</a>
                </li>
            </li>
        </ul>
    </div>
</nav>

<div id="editar_contrasena" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar Contraseña</h4>
            </div>
            <form id="formulario-contrasena" name="formulario-contrasena" autocomplete="off">
            <div class="modal-body">
                <input type="hidden" name="opcion" value="1">
                <div class="tab-content">
                    <div class="row separar">
                        <div class="col-md-6 text-center"><b>Ingrese Nueva Contraseña:</b></div>
                        <div class="col-md-6 text-left"><input type="password" class="form-control" id="contrasena1" name="contrasena1" required></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-6 text-center"><b>Repita Nueva Constraseña:</b></div>
                        <div class="col-md-6 text-left"><input type="password" class="form-control" id="contrasena2" name="contrasena2" required></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                    <button class="btn btn-success" type="submit" id="actualizar">Actualizar Contraseña</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<div id="reg_exito2" class="modal fade" role="dialog" data-backdrop="static" style="padding-top:15%;">
    <div class="modal-dialog modal-lg">
        <div id="alerta_mensaje2" class="alert alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            <div id="mensaje_respuesta2"></div>
        </div>
    </div>
</div>

<div id="contra"></div>