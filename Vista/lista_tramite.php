<?php include("includes/_cabecera.php")?>
<script Language="JavaScript">
$(document).ready(function() {
    var table = $("#tramitess").DataTable({
        //any other configuration options
        "ajax": {
            "url": "listar_tramites.php",
            "type": "POST"
        },
        "columns": [
            {"data": "Entidad", "className": "text-left"},
            {"data": "Tipo_Persona", "className": "text-center"},
            {"data": "Asunto", "className": "text-left"},
            {"data": "Tipo_Tramite", "className": "text-center"},
            {"data": "Fecha", "className": "text-center"},
            {"data": "Estado", "className": "text-center"},
            {"data": "Opc", "className": "text-center", "searchable":false}
        ],
        
        "ordering": false,
        "lengthMenu": [[25, 50, -1], [25, 50, "Todos"]]
    });

    setInterval(function(){
        table.ajax.reload();
    }, 180000);
});

function mostrar_evaluar(idTramite){
    $('#modal_evaluar').modal("show");
    $.ajax({
        url: '../Controlador/C_Tramite.php',
        type: 'POST',
        data: 'opcion=3&idTramite='+idTramite,
        success: function(data){
            $('#modal_evaluar').html(data);
        }
    });
}

function mostrar_detalle(idTramite){
    $('#modal_evaluar').modal("show");
    $.ajax({
        url: '../Controlador/C_Tramite.php',
        type: 'POST',
        data: 'opcion=9&idTramite='+idTramite,
        success: function(data){
            $('#modal_evaluar').html(data);
        }
    });
}
</script>
<style>
    .separar{margin:20px;}
    .separar2{margin-top:45px; margin-bottom:30px;}
</style>
</head>

<?php include("includes/_menu.php")?>

<div class="container" style="padding-top:50px;">
    <div class="panel panel-info">
    	<div class="panel-heading"><strong>LISTADO DE TRÁMITES PARA APROBACIÓN</strong></div>
    	<div class="panel-body">
            <table id="tramitess" class="table table-striped table-bordered" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">ENTIDAD / REPRESENTANTE</th>
                        <th class="text-center">TIPO PERSONA</th>
                        <th class="text-center">ASUNTO</th>
                        <th class="text-center">TIPO TRÁMITE</th>
                        <th class="text-center">FECHA REG.</th>
                        <th class="text-center">ESTADO</th>
                        <th class="text-center">OPC.</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div id="modal_evaluar" class="modal fade" role="dialog" data-backdrop="static"></div>
<div id="rechazo" class="modal fade" role="dialog" data-backdrop="static"></div>
<div id="apruebo" class="modal fade" role="dialog" data-backdrop="static"></div>

<div id="barra" class="modal fade" role="dialog" data-backdrop="static" style="padding-top:15%;">
	<div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <h4 class="modal-title">Registrando</h4>
            </div>
            <div class="modal-body">
            	<div class="tab-content">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="reg_exito" class="modal fade" role="dialog" data-backdrop="static" style="padding-top:15%;">
	<div class="modal-dialog modal-lg">
        <div id="alerta_mensaje" class="alert alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            <div id="mensaje_respuesta"></div>
        </div>
    </div>
</div>
<?php include("includes/_footer.php")?>
