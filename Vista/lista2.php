<?php include("includes/_cabecera.php")?>
<script Language="JavaScript">
$(document).ready(function() {
    $("#tramites").DataTable({
        "ordering": false,
        "lengthMenu": [[25, 50, -1], [25, 50, "Todos"]]
    });
    
    $('#fecha_ini, #fecha_fin').datetimepicker({
        locale: 'es',
        format: 'L'
    });
    
    $("#especialidad").select2({
        width:'100%'
    });
});

function mostrar_detalle(idTramite){
    $('#modal_evaluar').modal("show");
    $.ajax({
        url: '../Controlador/C_Tramite.php',
        type: 'POST',
        data: 'opcion=9&idTramite='+idTramite,
        success: function(data){
            $('#modal_evaluar').html(data);
        }
    });
}
</script>
<style>
    .separar{margin:20px;}
    .separar2{margin-top:45px; margin-bottom:30px;}
    .container{width:85%;}
</style>
</head>

<?php
include("includes/_menu.php");
if(!empty($_GET["fecha_ini"])){
    $fecha_ini=$_GET["fecha_ini"];
    $fi=explode("/", $fecha_ini);
    $Desde=$fi[2]."-".$fi[1]."-".$fi[0];
}else{
    $fecha_ini="";
    $fi="";
    $Desde="";
}
if(!empty($_GET["fecha_fin"])){
    $fecha_fin=$_GET["fecha_fin"];
    $ff=explode("/", $fecha_fin);
    $Hasta=$ff[2]."-".$ff[1]."-".$ff[0];
}else{
    $fecha_fin="";
    $ff="";
    $Hasta="";
}
if(!empty($_GET["estado"])){$estado=$_GET["estado"];}else{$estado="";}
if(!empty($_GET["tipo_tramite"])){$tipo_tramite=$_GET["tipo_tramite"];}else{$tipo_tramite="";}
?>

<div class="container" style="padding-top:50px;">
    <div class="panel panel-info">
    	<div class="panel-heading"><strong>LISTADO TOTAL DE TRÁMITES VIRTUALES</strong></div>
    	<div class="panel-body">
            <form method="get">
                <div class="row" style="height:60px;">
                    <div class="col-md-2 col-xs-2 text-right">Fec. Reg. Inicio</div>
                    <div class="col-md-2 col-xs-2"><input type="text" class="form-control" name="fecha_ini" id="fecha_ini" placeholder="DD/MM/YYYY" maxlength="10" value="<?=$fecha_ini;?>" size="5"></div>
                    <div class="col-md-2 col-xs-2 text-right">Fec. Reg. Fin</div>
                    <div class="col-md-2 col-xs-2"><input type="text" class="form-control" name="fecha_fin" id="fecha_fin" placeholder="DD/MM/YYYY" maxlength="10" value="<?=$fecha_fin;?>" size="5"></div>
                    <div class="col-md-2 col-xs-2 text-right">Estado:</div>
                    <div class="col-md-2 col-xs-2">
                        <select class="form-control" id="estado" name="estado">
                            <option value="">Seleccione:</option>
                            <option value="1" <?php if($estado == '1'){echo "selected";}?>>PENDIENTE</option>
                            <option value="2" <?php if($estado == '2'){echo "selected";}?>>APROBADO</option>
                            <option value="3" <?php if($estado == '3'){echo "selected";}?>>RECHAZADO</option>
                        </select>
                    </div>
                </div>
                <div class="row" style="height:60px;">
                    <div class="col-md-2 col-xs-2 text-right">Tipo Trámite</div>
                    <div class="col-md-2 col-xs-2">
                        <select class="form-control" id="tipo_tramite" name="tipo_tramite">
                            <option value="">Seleccione:</option>
                            <?php
                            $sql2="SELECT cDocuType, sDocuDesc FROM Tipo_Documento WHERE cNumbFlag = '0' ORDER BY sDocuDesc ASC";
                            $Tra=$datos->listar($sql2);
                            foreach($Tra as $tra):
                            ?>
                            <option value="<?=$tra["cDocuType"]?>"><?=strtoupper(utf8_encode($tra["sDocuDesc"]));?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="col-md-2 col-xs-2 text-right"></div>
                    <div class="col-md-6 col-xs-6 text-center">
                        <button class="btn btn-success btn-lg" type="submit">Buscar</button>
                    </form>
                        <!--<button class="btn btn-primary btn-lg" type="button" onClick="location.href='reporte_excel.php?fecha_ini=<?=$fecha_ini?>&fecha_fin=<?=$fecha_fin?>&estado=<?=$estado?>&establecimiento=<?=$establecimiento?>'">Exportar a excel <span class="glyphicon glyphicon-floppy-disk"></span></button>!-->
                        <button class="btn btn-danger btn-lg" type="button" onClick="location.href='lista2.php'">Reestablecer</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table id="tramites" class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center">ENTIDAD / REPRESENTANTE</th>
                                    <th class="text-center">TIPO PERSONA</th>
                                    <th class="text-center">TIPO TRÁMITE</th>
                                    <th class="text-center">ASUNTO</th>
                                    <th class="text-center">FECHA REG.</th>
                                    <th class="text-center">ESTADO</th>
                                    <th class="text-center">MOTIVO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sql="SELECT ta.idTramite, ta.asunto, ta.tipo_persona, ta.entidad, ta.representante, CONVERT(nvarchar, ta.fechaRegistro, 120) AS Fecha, ta.estado, CONVERT(nvarchar, ta.fechaEvaluacion, 120) AS Fec_Eva, ta.observaciones, ta.mensaje_respuesta, td.cDocuType, td.sDocuDesc FROM tramite_aprobar ta LEFT JOIN Tipo_Documento td ON ta.tipo_tramite = td.cDocuType WHERE 1 = 1 ";
                                if(!empty($fecha_ini) && empty($fecha_fin)){
                                    $sql.="AND ta.fechaRegistro >= CONVERT(date, '$Desde') ";
                                }
                                if(empty($fecha_ini) && !empty($fecha_fin)){
                                    $sql.="AND ta.fechaRegistro <= CONVERT(date, '$Hasta') ";
                                }
                                if(!empty($fecha_ini) && !empty($fecha_fin)){
                                    $sql.="AND ta.fechaRegistro BETWEEN CONVERT(date, '$Desde') AND CONVERT(date, '$Hasta')";
                                }
                                if($estado != ""){
                                    $sql.="AND ta.estado = '$estado' ";
                                }
                                if($tipo_tramite != ""){
                                    $sql.="AND ta.tipo_tramite = '$tipo_tramite' ";
                                }
                                $sql.="ORDER BY ta.idTramite DESC";
                                $rs=$datos->listar($sql);//echo $sql;
                                foreach($rs as $Rs):
                                    $entidad=htmlspecialchars($Rs['entidad'], ENT_NOQUOTES, "UTF-8");
                                    $representante=htmlspecialchars($Rs['representante'], ENT_NOQUOTES, "UTF-8");
                                    $cadena="<b>".$entidad."</b><br />".$representante;

                                    if($Rs["tipo_persona"]=="1"){
                                        $tipPer="PERSONA NATURAL";
                                    }if($Rs["tipo_persona"]=="2"){
                                        $tipPer="ENTIDAD PÚBLICA O PRIVADA";
                                    }
									
									
									//$date = new DateTime($Rs['Fec_Eva']);
									//echo $date->format('Y-m-d H:i:s');
									//$var=$Rs['Fec_Eva']->format('d-m-Y H:i:s');
									//$var=date_create($Rs['Fec_Eva'])->format('Y-m-d');
									if($Rs["Fec_Eva"] != NULL){
										$var=date("d-m-Y H:i", strtotime($Rs['Fec_Eva']));
									}else{
										$var="";
									}
									
                                    if($Rs["estado"]==1){
                                        //$estado="<span style='font-weight: bold;' class='label label-warning'>PENDIENTE</span>";
                                        $estado="<div class='alert alert-warning'>PENDIENTE</div>";
                                        $mensaje="";
                                    }elseif($Rs["estado"]==2){
                                        //$estado="<span style='font-weight: bold;' class='label label-success'>APROBADO</span>";
                                        $estado="<div class='alert alert-success'>APROBADO<br />".$var."</div>";
                                        $mensaje=$Rs["mensaje_respuesta"];
                                    }elseif($Rs["estado"]==3){
                                        //$estado="<span style='font-weight: bold;' class='label label-danger'>RECHAZADO</span>";
                                        $estado="<div class='alert alert-danger'>RECHAZADO<br />".$var."</div>";
                                        $mensaje=$Rs["observaciones"];
                                    }

                                    /*if($Rs['estado'] == '1'){
                                        $boton = "<span class='glyphicon glyphicon-pencil' aria-hidden='true' data-toggle='tooltip' title='Evaluar Trámite' onClick='mostrar_evaluar(".$Rs['idTramite'].");'></span>";
                                    }elseif($Rs['estado'] == '2' || $Rs['estado'] == '3'){*/
                                        $boton = "<span class='glyphicon glyphicon-list-alt' aria-hidden='true' data-toggle='tooltip' title='Detalle de Trámite' onClick='mostrar_detalle(".$Rs['idTramite'].");'></span>";
                                    //}
                                ?>
                                <tr>
                                    <td align="left" style="vertical-align:middle;"><?=$cadena?></td>
                                    <td align="center" style="vertical-align:middle;"><?=$tipPer?></td>
                                    <td align="center" style="vertical-align:middle;"><?=$Rs['sDocuDesc']?></td>
                                    <td align="left" style="vertical-align:middle;"><?=$Rs["asunto"]?></td>
                                    <td align="center" style="vertical-align:middle;"><?=date("d-m-Y H:i", strtotime($Rs['Fecha']))?></td>
                                    <td align="center" style="vertical-align:middle;"><?=$estado?></td>
                                    <td align="center" style="vertical-align:middle;"><?=$mensaje?></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>

<div id="modal_evaluar" class="modal fade" role="dialog" data-backdrop="static"></div>
<?php include("includes/_footer.php")?>
