<?php include("includes/_cabecera.php")?>
<script language="javascript">
    $(document).ready(function(){
        $("#distrito").select2({
            width:'100%'
        });

        $('#nrodocumento, #nrodocumentoa, #celular, #tel_casa').numeric();

        $("#nrodocumento").prop("disabled", true);
        //validar cantidad de digitos de numero de documento
        $("#tipo_documento").change(function () {
            $("#tipo_documento option:selected").each(function() {
                tipo=$(this).val();
                if(tipo=="DNI"){
                    $("#nrodocumento").prop('required',true);
                    $("#nrodocumento").prop('disabled',false);
                    $("#nrodocumento").attr("maxlength", 8);
                }else if(tipo=="CE" || tipo=="PAS"){
                    $("#nrodocumento").prop('required',true);
                    $("#nrodocumento").prop('disabled',false);
                    $("#nrodocumento").attr("maxlength", 15);
                }else{
                    $("#nrodocumento").prop('required',true);
                    $("#nrodocumento").prop('readonly',true);
                    $('#nrodocumento').val('');
                }
            });
        });

        $("#fecha_nac").keyup(function () {
            cadena=$(this).val();
            subcadena=cadena.split("/");
            if(cadena.length==2 && cadena.indexOf('/')==-1){
                $("#fecha_nac").val(cadena + "/");
            }else if(cadena.length==5 && subcadena.length==2 && subcadena.indexOf('/')==-1){
                $("#fecha_nac").val(cadena + "/");
            }
        });

        $("#fecha_nac").focusout(function () {
            fecha_nac=$(this).val();
            $.post("../Controlador/C_Pacientes.php", { fecha_nac: fecha_nac, opcion: "2" }, function(data){
                $("#edad").html(data);
            });
        });

        $("#formulario-paciente").submit(function(event){
            var dataString = $("#formulario-paciente").serialize();
            $.ajax({
                type: "POST",
                url: "../Controlador/C_Pacientes.php",
                data: dataString,
                beforeSend: function(){
                    $('#registrar').prop('disabled',true);
                    $('#nuevo_paciente').modal("hide");
                    $('#barra').modal("show");
                },
                success: function(data){
                    $('#barra').modal("hide");
                    $('#registrar').prop('disabled',false);
                    //$('#nuevo_expediente').modal("show");
                    $('#pac').html(data);
                }
            })
            event.preventDefault();
        });

        $("#tipo_documento").change(function () {
            $("#tipo_documento option:selected").each(function () {
                tipo_documento=$(this).val();
                $.post("../Controlador/C_Pacientes.php", { tipo_documento: tipo_documento, opcion: "4" }, function(data){
                    $("#nrodocumento").val(data);
                });     
            });
        });

        $(".close").click(function(){
            $("#formulario-paciente")[0].reset();
        });

        $("#apoderado").hide('fast');

        mostrar_listado();
    });
    function mostrar_listado(){
        $.ajax({
            url: '../Controlador/C_Pacientes.php',
            type: 'POST',
            data: 'opcion=1',
            success: function(data){
                $('#pac').html(data);
            }
        });
    }
</script>
<style>
    .separar{margin:20px;}
    .separar2{margin-top:45px; margin-bottom:30px;}
</style>
</head>

<body style="background-color: #E4FFFF;">
<?php include("includes/_menu.php")?>

<div class="container" style="padding-top:80px;">
    <div class="panel panel-info">
    	<div class="panel-heading"><strong>PACIENTES</strong></div>
    	<div class="panel-body">
            <div class="row text-center">
                <div class="col-md-12">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#nuevo_paciente">Nuevo Paciente</button>
                    <button class="btn btn-danger" onClick="location.href='main.php'">Reestablecer</button>
                </div>
            </div>
            <br />
            <div id="pac"></div>
        </div>
    </div>
</div>

<div id="editar_historia" class="modal fade" role="dialog" data-backdrop="static"></div>

<div id="nuevo_paciente" class="modal fade" role="dialog" data-backdrop="static">
	<div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nuevo Paciente</h4>
            </div>
            <form id="formulario-tramite" name="formulario-tramite" autocomplete="off">
            <div class="modal-body">
                <input type="hidden" name="opcion" value="3">
                <div class="tab-content">
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Tipo Documento:</b></div>
                        <div class="col-md-3">
                            <select class="form-control" id="tipo_documento" name="tipo_documento" required>
                                <option value="">Seleccione:</option>
                                <option value="DNI">DNI</option>
                                <option value="CE">Carnet Extranjería</option>
                                <option value="PAS">Pasaporte</option>
                                <option value="SD">Sin Documento</option>
                            </select>
                        </div>
                        <div class="col-md-3 text-right"><b>N&deg; Documento:</b></div>
                        <div class="col-md-3"><input type="text" class="form-control" id="nrodocumento" name="nrodocumento"></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Apellido Paterno:</b></div>
                        <div class="col-md-3"><input type="text" class="form-control text-uppercase" id="ape_paterno" name="ape_paterno" required></div>
                        <div class="col-md-3 text-right"><b>Apellido Materno:</b></div>
                        <div class="col-md-3"><input type="text" class="form-control text-uppercase" id="ape_materno" name="ape_materno" required></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Nombres:</b></div>
                        <div class="col-md-5"><input type="text" class="form-control text-uppercase" id="nombres" name="nombres" required></div>
                        <div class="col-md-1 text-right"><b>Sexo:</b></div>
                        <div class="col-md-3">
                            <select class="form-control" id="sexo" name="sexo" required>
                                <option value="">Seleccione:</option>
                                <option value="M">MASCULINO</option>
                                <option value="F">FEMENINO</option>
                            </select>
                        </div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Fecha Nacimiento:</b></div>
                        <div class="col-md-3"><input type="text" class="form-control text-uppercase" id="fecha_nac" name="fecha_nac" required placeholder="dd/mm/YYYY" maxlength="10"></div>
                        <div class="col-md-6 text-center"><div id="edad" style="font-weight: bold;"></div></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Estado Civil:</b></div>
                        <div class="col-md-3">
                            <select class="form-control" id="estado_civil" name="estado_civil">
                                <option value="">Seleccione:</option>
                                <option value="1">Soltero</option>
                                <option value="2">Casado</option>
                                <option value="3">Conviviente</option>
                                <option value="4">Divorciado</option>
                                <option value="5">Separado</option>
                                <option value="6">Viudo</option>
                            </select>
                        </div>
                        <div class="col-md-3 text-right"><b>Nº Celular:</b></div>
                        <div class="col-md-3"><input type="text" class="form-control" id="celular" name="celular" maxlength="9"></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Nº Familiar:</b></div>
                        <div class="col-md-3"><input type="text" class="form-control" id="num_familiar" name="num_familiar" maxlength="9"></div>
                        <div class="col-md-3 text-right"><b>Teléfono Casa:</b></div>
                        <div class="col-md-3"><input type="text" class="form-control" id="tel_casa" name="tel_casa" maxlength="9"></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Correo:</b></div>
                        <div class="col-md-9"><input type="text" class="form-control" id="correo" name="correo"></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Distrito:</b></div>
                        <div class="col-md-9">
                            <select class="form-control" id="distrito" name="distrito">
                                <option value="">Seleccione:</option>
                                <?php
                                $sql3="SELECT cod_dist, desc_dist FROM dist ORDER BY desc_dist ASC";
                                $Dep=$datos->listar($sql3);
                                foreach($Dep as $dep):
                                ?>
                                <option value="<?=$dep["cod_dist"]?>"><?=strtoupper(utf8_encode($dep["desc_dist"]));?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Dirección:</b></div>
                        <div class="col-md-9"><input type="text" class="form-control text-uppercase" id="direccion" name="direccion"></div>
                    </div>
                    <div class="row separar">
                        <div class="col-md-3 text-right"><b>Referencia:</b></div>
                        <div class="col-md-9"><input type="text" class="form-control text-uppercase" id="referencia" name="referencia"></div>
                    </div>
                    <div id="apoderado">
                        <h4>Datos del Apoderado</h4>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Nombres y Apellidos:</b></div>
                            <div class="col-md-5"><input type="text" class="form-control text-uppercase" id="apoderado" name="apoderado"></div>
                            <div class="col-md-2 text-right"><b>Parentesco:</b></div>
                            <div class="col-md-2">
                                <select class="form-control" id="parentesco" name="parentesco">
                                    <option value="">Seleccione:</option>
                                    <option value="PADRE">PADRE</option>
                                    <option value="MADRE">MADRE</option>
                                    <option value="HERMANO(A)">HERMANO(A)</option>
                                    <option value="TIO(A)">TIO(A)</option>
                                    <option value="ABUELO(A)">ABUELO(A)</option>
                                    <option value="PRIMO(A)">PRIMO(A)</option>
                                    <option value="ENCARGADO(A)">ENCARGADO(A)</option>
                                </select>
                            </div>
                        </div>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Nº Documento:</b></div>
                            <div class="col-md-3"><input type="text" class="form-control" id="nrodocumentoa" name="nrodocumentoa" maxlength="8"></div>
                            <div class="col-md-6 text-center"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="text-center">
                	<button class="btn btn-primary" type="submit" id="registrar">Registrar Paciente</button>
                </div>
            </div>
            </form>
        </div>
  	</div>
</div>

<div id="historial_historia" class="modal fade" role="dialog" data-backdrop="static"></div>
<div id="historial" class="modal fade" role="dialog" data-backdrop="static"></div>
<div id="mostrar_editar" class="modal fade" role="dialog" data-backdrop="static"></div>

<div id="barra" class="modal fade" role="dialog" data-backdrop="static" style="padding-top:15%;">
	<div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <h4 class="modal-title">Registrando</h4>
            </div>
            <div class="modal-body">
            	<div class="tab-content">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="reg_exito" class="modal fade" role="dialog" data-backdrop="static" style="padding-top:15%;">
	<div class="modal-dialog modal-lg">
        <div id="alerta_mensaje" class="alert alert-dismissable fade in">
            <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            <div id="mensaje_respuesta"></div>
        </div>
    </div>
</div>

<?php include("includes/_footer.php")?>
