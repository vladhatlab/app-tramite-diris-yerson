<?php
session_start();
header("Content-Type: text/html; charset=utf-8");
require_once("../Modelo/M_Datos.php");
$datos=new M_Datos();

$tabla="";
$sql="SELECT ta.idTramite, ta.asunto, ta.tipo_persona, ta.entidad, ta.representante, CONVERT(nvarchar, ta.fechaRegistro, 120) AS Fecha, ta.estado, td.sDocuDesc FROM tramite_aprobar ta LEFT JOIN Tipo_Documento td ON ta.tipo_tramite = td.cDocuType WHERE ta.estado = '1' ORDER BY ta.idTramite ASC";
$rs=$datos->listar($sql);//echo $sql;
					
	foreach($rs as $Rs):
		//validacion de opciones
		if($Rs['estado'] == '1'){
			$boton = "<span class='glyphicon glyphicon-pencil' aria-hidden='true' data-toggle='tooltip' title='Evaluar Trámite' onClick='mostrar_evaluar(".$Rs['idTramite'].");'></span>";
		}elseif($Rs['estado'] == '2' || $Rs['estado'] == '3'){
			$boton = "<span class='glyphicon glyphicon-list-alt' aria-hidden='true' data-toggle='tooltip' title='Detalle de Trámite' onClick='mostrar_detalle(".$Rs['idTramite'].");'></span>";
		}
        
        $entidad=htmlspecialchars($Rs['entidad'], ENT_NOQUOTES, "UTF-8");
        $representante=htmlspecialchars($Rs['representante'], ENT_NOQUOTES, "UTF-8");
        $asunto=htmlspecialchars($Rs['asunto'], ENT_NOQUOTES, "UTF-8");
        $sDocuDesc=htmlspecialchars($Rs['sDocuDesc'], ENT_NOQUOTES, "UTF-8");

        if($Rs["tipo_persona"]=="1"){
			$tipPer="PERSONA NATURAL";
		}if($Rs["tipo_persona"]=="2"){
			$tipPer="ENTIDAD PÚBLICA O PRIVADA";
		}

		//mostrar entidad o representante
		if($Rs["tipo_persona"]=="1"){
			$cadena = $representante;
		}elseif($Rs["tipo_persona"]=="2"){
			$cadena = "<b>".$entidad."</b><br />".$representante;
		}

		//mostrar estado
		$estado="";
		if($Rs["estado"]==1){
            $estado="<span style='color: #FFBD4B; font-weight: bold;'>PENDIENTE</span>";
        }elseif($Rs["estado"]==2){
            $estado="<span style='color: #4C915A; font-weight: bold;'>APROBADO</span>";
        }elseif($Rs["estado"]==3){
            $estado="<span style='color: #F33942; font-weight: bold;'>RECHAZADO</span>";
        }
		
		//formatear fechaRegistro
		$Fec=date("d-m-Y H:i", strtotime($Rs['Fecha']));

		//mostrar grilla
		$tabla.='{
			"Entidad":"'.$cadena.'",
		  	"Tipo_Persona":"'.$tipPer.'",
		  	"Asunto":"'.$asunto.'",
		  	"Tipo_Tramite":"'.$sDocuDesc.'",
		  	"Fecha":"'.$Fec.'",
		  	"Estado":"'.$estado.'",
		  	"Opc":"'.$boton.'"
		},';
	endforeach;
					
	//eliminamos la coma que sobra
	$tabla = substr($tabla,0, strlen($tabla) - 1);
	
	//Creamos el JSON
	echo '{"data":['.$tabla.']}';
?>