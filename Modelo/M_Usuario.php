<?php
//session_start();
require_once("M_Datos.php");
class M_Usuario{
	private $lista;
	
	public function __construct(){
		date_default_timezone_set('America/Lima');
		$this->objDatos=new M_Datos();
	}

	function cambiar_contrasena($contrasena1, $contrasena2){
		if($contrasena1 == $contrasena2){
			$ncontra=sha1(sha1($contrasena1));
			$upd="UPDATE usuarios SET contrasena = '$ncontra' WHERE idUsuario = '$_SESSION[idUsuario]'";
			$this->objDatos->ejecutar($upd);//echo $upd;
			echo "
			<script language='JavaScript'>
				$('#reg_exito2').modal('show');
				$('#alerta_mensaje2').removeClass('alert-danger').addClass('alert-success');
				$('#mensaje_respuesta2').html('<strong>Contraseña actualizada satisfactoriamente</strong>');
			</script>
			";
		}else{
			echo "
			<script language='JavaScript'>
				$('#reg_exito2').modal('show');
				$('#alerta_mensaje2').removeClass('alert-success').addClass('alert-danger');
				$('#mensaje_respuesta2').html('<strong>Las contraseñas no coinciden</strong>');
			</script>
			";
		}
	}

	function mostrar_listado(){
		$sql="SELECT idUsuario, CONCAT(ape_paterno, ' ', ape_materno, ' ', nombres) AS Nombres, (SELECT descripcion FROM maestra WHERE codigo = 3 AND codigo_valor = usuarios.perfil) AS Perfil, (SELECT nombre FROM local WHERE idLocal = usuarios.sucursal) AS Local, usuario, estado FROM usuarios ORDER BY estado ASC, ape_paterno ASC";
		$rs=$this->objDatos->listar($sql);
	?>
	<script language='JavaScript'>
        $(document).ready(function() {
            var table = $("#usuarios").DataTable({
                "ordering": false,
                "lengthMenu": [[25, 50, -1], [25, 50, "Todos"]]
            });
        });

        function edicion_usuario(idUsuario){
            $('#editar_usuario').modal("show");
            $.ajax({
                url: '../Controlador/C_Usuarios.php',
                type: 'POST',
                data: 'opcion=4&idUsuario='+idUsuario,
                success: function(data){
                    $('#editar_usuario').html(data);
                }
            });
        }

        function cambiar_estado(idUsuario, estado){
            $('#barra').modal("show");
            $.ajax({
                url: '../Controlador/C_Usuarios.php',
                type: 'POST',
                data: 'opcion=6&idUsuario='+idUsuario+'&estado='+estado,
                success: function(data){
                	$('#barra').modal("hide");
                    $('#usu').html(data);
                }
            });
        }

        $('[data-toggle="tooltip"]').tooltip();
    </script>
	<table id="usuarios" class="table table-striped table-bordered" width="100%">
        <thead>
            <tr>
                <th class="text-center">NOMBRES</th>
                <th class="text-center">USUARIO</th>
                <th class="text-center">PERFIL</th>
                <th class="text-center">LOCAL</th>
                <th class="text-center">ESTADO</th>
                <th class="text-center">OPC.</th>
            </tr>
        </thead>

        <tbody>
            <?php 
            	foreach($rs as $Rs){
                    if($Rs["estado"]=='1'){
                        $estado="<b style='color:green;'>ACTIVO</b>";
                    }elseif($Rs["estado"]=='2'){
                        $estado="<b style='color:red;'>INACTIVO</b>";
                    }
            ?>
                <tr>
                    <td class="text-center"><?=$Rs["Nombres"];?></td>
                    <td class="text-center"><?=$Rs["usuario"];?></td>
                    <td class="text-center"><?=utf8_encode($Rs["Perfil"]);?></td>
                    <td class="text-center"><?=$Rs["Local"];?></td>
                    <td class="text-center"><?=$estado;?></td>
                    <td class="text-center">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle='tooltip' title="Editar Usuario" onClick="edicion_usuario(<?=$Rs['idUsuario'];?>);"></span>&nbsp;
                        <?php
                        	if($Rs["estado"] == 1){
                        		$imagen = "glyphicon glyphicon-ban-circle";
                        		$title = "Inhabilitar Usuario";
                        	}elseif($Rs["estado"] == 2){
                        		$imagen = "glyphicon glyphicon-ok-circle";
                        		$title = "Habilitar Usuario";
                        	}
                        ?>
                        <span class="<?=$imagen?>" aria-hidden="true" data-toggle='tooltip' title="<?=$title?>" onClick="cambiar_estado(<?=$Rs['idUsuario']?>,<?=$Rs['estado']?>);"></span>
                    </td>
                </tr>
            <?php }?>
        </tbody>
    </table>

	<?php
	}

	function registrar_usuario($ape_paterno, $ape_materno, $nombres, $perfil, $local, $usuario, $contrasena){
		$ins = "INSERT INTO usuarios (nombres, ape_paterno, ape_materno, perfil, sucursal, usuario, contrasena, estado) ";
		$ins .= "VALUES ";
		$ins .= "('$nombres', '$ape_paterno', '$ape_materno', '$perfil', '$local', '$usuario', '$contrasena', '1')";
		$this->objDatos->ejecutar($ins);

		echo "
        <script Language='JavaScript'>
            $(document).ready(function() {
                $('#reg_exito').modal('show');
                $('#alerta_mensaje').removeClass('alert-danger').addClass('alert-success');
                $('#mensaje_respuesta').html('<b>Registro guardado satisfactoriamente</b>');
            });
        </script>
        ";

        //actualizar listado
        $this->mostrar_listado();

        //limpiar formulario
        echo "
        <script Language='JavaScript'>
            $(document).ready(function() {
                $('#formulario-usuario')[0].reset();
            });
        </script>
        ";
	}

	function mostrar_edicion($idUsuario){
		$sql = "SELECT * FROM usuarios WHERE idUsuario = '$idUsuario'";
		$rs=$this->objDatos->listar($sql);
		?>
		<script Language="JavaScript">
		$(document).ready(function() {
		  	$('#nrodocumento').numeric();

			$("#formulario-usuario-editar").submit(function(event){
				var dataString = $("#formulario-usuario-editar").serialize();
				$.ajax({
					type: "POST",
					url: "../Controlador/C_Usuarios.php",
					data: dataString,
					beforeSend: function(){
						$('#editar_usuario').modal("hide");
						$('#barra').modal("show");
					},
					success: function(data){
						$('#barra').modal("hide");
						$('#reg_exito').modal("show");
						$('#usu').html(data);
					}
				})
				event.preventDefault()
			});

		    $(".close").click(function(){
		        $("#formulario-usuario")[0].reset();
		    });
		});
		</script>
		<style>
		    .separar{margin:20px;}
		    .separar2{margin-top:45px; margin-bottom:30px;}
		</style>
		<div class="modal-dialog modal-lg">
	        <!-- Modal content-->
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	                <h4 class="modal-title">Actualizar Usuario</h4>
	            </div>
	            <form role="form" id="formulario-usuario-editar" name="formulario-usuario-editar" method="post" autocomplete="off">
	            <input type="hidden" name="idUsuario" id="idUsuario" value="<?=$rs[0]['idUsuario'];?>">
	            <div class="modal-body">
	                <input type="hidden" name="opcion" value="5">
	                <div class="tab-content">
	                    <div class="row separar">
	                        <div class="col-md-3 text-right"><b>Apellidos Paterno:</b></div>
	                        <div class="col-md-3"><input type="text" class="form-control text-uppercase" id="ape_paterno" name="ape_paterno" value="<?=$rs[0]['ape_paterno']?>" required></div>
	                        <div class="col-md-3 text-right"><b>Apellidos Materno:</b></div>
	                        <div class="col-md-3"><input type="text" class="form-control text-uppercase" id="ape_materno" name="ape_materno" value="<?=$rs[0]['ape_materno']?>" required></div>
	                    </div>
	                    <div class="row separar">
	                        <div class="col-md-3 text-right"><b>Nombres:</b></div>
	                        <div class="col-md-9"><input type="text" class="form-control text-uppercase" id="nombres" name="nombres" value="<?=$rs[0]['nombres']?>" required></div>
	                    </div>
	                    <div class="row separar">
	                        <div class="col-md-3 text-right"><b>Local:</b></div>
	                        <div class="col-md-3">
	                        	<select class="form-control" id="local" name="local" required>
	                                <option>Seleccione:</option>
	                                <?php
	                                $sql2="SELECT idLocal, nombre FROM local ORDER BY idLocal ASC";
	                                $Loc=$this->objDatos->listar($sql2);
	                                foreach($Loc as $loc):
	                                	if($loc["idLocal"] == $rs[0]["sucursal"]){
	                                		$sel2="selected";
	                                	}else{
	                                		$sel2="";
	                                	}
	                                ?>
	                                <option value="<?=$loc["idLocal"]?>" <?=$sel2;?>><?=utf8_encode($loc["nombre"]);?></option>
	                            	<?php endforeach;?>
	                            </select>
	                        </div>
	                        <div class="col-md-3 text-right"><b>Perfil:</b></div>
	                        <div class="col-md-3">
	                        	<select class="form-control" id="perfil" name="perfil" required>
	                                <option>Seleccione:</option>
	                                <?php
	                                $sql="SELECT codigo_valor, descripcion FROM maestra WHERE codigo = '3' ORDER BY descripcion ASC";
	                                $Per=$this->objDatos->listar($sql);
	                                foreach($Per as $per):
	                                	if($per["codigo_valor"] == $rs[0]["perfil"]){
	                                		$sel="selected";
	                                	}else{
	                                		$sel="";
	                                	}
	                                ?>
	                                <option value="<?=$per["codigo_valor"];?>" <?=$sel;?>><?=utf8_encode($per["descripcion"]);?></option>
	                            	<?php endforeach;?>
	                            </select>
	                       	</div>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	            	<div class="text-center">
	                	<button class="btn btn-primary" type="submit">Actualizar Usuario</button>
	                </div>
	            </div>
	            </form>
	        </div>
	  	</div>
		<?php

	}

	function editar_usuario($ape_paterno, $ape_materno, $nombres, $perfil, $local, $idUsuario){
		$upd="UPDATE usuarios SET ";
        $upd.="ape_paterno = '$ape_paterno', ";
        $upd.="ape_materno = '$ape_materno', ";
        $upd.="nombres = '$nombres', ";
        $upd.="perfil = '$perfil', ";
        $upd.="sucursal = '$local' ";
        $upd.="WHERE idUsuario = '$idUsuario' ";
        $this->objDatos->ejecutar($upd);

        echo "
        <script Language='JavaScript'>
            $(document).ready(function() {
                $('#reg_exito').modal('show');
                $('#alerta_mensaje').removeClass('alert-danger').addClass('alert-success');
                $('#mensaje_respuesta').html('<b>Registro actualizado satisfactoriamente</b>');
            });
        </script>
        ";

        //actualizar listado
        $this->mostrar_listado();

        //limpiar formulario
        echo "
        <script Language='JavaScript'>
            $(document).ready(function() {
                $('#formulario-usuario')[0].reset();
            });
        </script>
        ";

	}

	function cambiar_estado($idUsuario, $estado){
		if($estado == '1'){
			$ne = '2';
		}elseif($estado == '2'){
			$ne = '1';
		}

		$upd = "UPDATE usuarios SET estado = '$ne' WHERE idUsuario = '$idUsuario'";
		$this->objDatos->ejecutar($upd);

		//actualizar listado
        $this->mostrar_listado();
	}
}
?>