<?php
require_once("M_Datos.php");
class M_Tramite
{
    private $lista;

    public function __construct()
    {
        date_default_timezone_set('America/Lima');
        $this->objDatos = new M_Datos();
    }

    function registrar_tramite($tipo_tramite, $asunto, $tipo_persona, $entidad, $representante, $tipo_documento, $nrodocumento, $correo, $telefono, $sumilla, $archivo_entidad)
    {
        $sql = "INSERT INTO tramite_aprobar (tipo_tramite, asunto, tipo_persona, entidad, representante, tipo_documento, nrodocumento, correo, telefono, sumilla, archivo_entidad, estado) VALUES ('$tipo_tramite', '$asunto', '$tipo_persona', '$entidad', '$representante', '$tipo_documento', '$nrodocumento', '$correo', '$telefono', '$sumilla', '$archivo_entidad', '1')";


        $resultado = $this->objDatos->ejecutar($sql);

        if ($resultado == "ok") {
            
            echo '<div class="modal-dialog">
    
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h2 class="modal-title text-center"><b>REGISTRO SATISFACTORIO</b></h2>
                            </div>
                            <div class="modal-body">
                                <div class="row separar">
                                    <div class="col-md-12 text-center"><b>Su trámite ha sido registrado con éxito para su evaluación.<br />En las próximas horas recibirá un mensaje en su correo con los pasos respectivos para hacer su seguimiento.</b></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <div class="text-center">
                                    <a class="btn btn-danger" href="index.php" type="submit">Cerrar Ventana</a>
                                </div>
                            </div>
                        </div>
                   </div>';
        } else {
            echo '<div class="modal-dialog">
    
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h2 class="modal-title text-center"><b>ERROR EN EL REGISTRO</b></h2>
                                </div>
                                <div class="modal-body">
                                    <div class="row separar">
                                        <div class="col-md-12 text-center" style="font-size:20px;">Por favor, vuelva a intentarlo</div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <div class="text-center">
                                        <a class="btn btn-danger" href="index.php" type="submit">Cerrar Ventana</a>
                                    </div>
                                </div>
                            </div>
                   </div>';
        }

?>


    <?php
    }

    function mostrar_evaluar($idTramite)
    {
        $sql = "SELECT ta.idTramite, ta.asunto, ta.tipo_persona, ta.entidad, ta.representante, ta.nrodocumento, ta.correo, ta.telefono, ta.sumilla, ta.archivo_entidad, td.sDocuDesc, oe.TipDocPerTDescrip FROM tramite_aprobar ta 
			LEFT JOIN Tipo_Documento td ON ta.tipo_tramite = td.cDocuType
			LEFT JOIN oeimTipDocPersonal oe ON ta.tipo_documento = oe.TipDocPerCCod
			WHERE idTramite = '$idTramite'";
        $rs = $this->objDatos->listar($sql);

        $archivo = utf8_encode($rs[0]["archivo_entidad"]);

        $ruta = "adjuntos/" . $archivo;
    ?>
        <script language='JavaScript'>
            function rechazar(idTramite) {
                $('#rechazo').modal("show");
                $.ajax({
                    url: '../Controlador/C_Tramite.php',
                    type: 'POST',
                    data: 'opcion=4&idTramite=' + idTramite,
                    success: function(data) {
                        $('#rechazo').html(data);
                    }
                });
            }

            function aprobar(idTramite) {
                $('#apruebo').modal("show");
                $.ajax({
                    url: '../Controlador/C_Tramite.php',
                    type: 'POST',
                    data: 'opcion=6&idTramite=' + idTramite,
                    success: function(data) {
                        $('#apruebo').html(data);
                    }
                });
            }

            $('[data-toggle="tooltip"]').tooltip();
        </script>
        <style>
            .separar {
                margin: 20px;
            }

            .separar2 {
                margin-top: 45px;
                margin-bottom: 30px;
            }
        </style>
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle del Trámite</h4>
                </div>
                <form id="formulario-paciente-editar" name="formulario-paciente-editar" autocomplete="off">
                    <div class="modal-body">
                        <input type="hidden" name="opcion" value="7">
                        <input name="cliente_nombre" id="cliente_nombre" value="<?= $rs[0]["representante"] ?>" hidden>
                        <input name="cliente_dni" id="cliente_dni" value="<?= $rs[0]["nrodocumento"] ?>" hidden>
                        <input name="cliente_correo" id="cliente_correo" value="<?= $rs[0]["correo"] ?>" hidden>

                        <div class="tab-content">
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Tipo Trámite:</b></div>
                                <div class="col-md-9 text-left"><?= $rs[0]["sDocuDesc"] ?></div>
                            </div>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Entidad:</b></div>
                                <div class="col-md-9 text-left"><?= stripslashes($rs[0]["entidad"]) ?></div>
                            </div>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Representante:</b></div>
                                <div class="col-md-3"><?= stripslashes($rs[0]["representante"]) ?></div>
                                <div class="col-md-3 text-right"><b><?= $rs[0]["TipDocPerTDescrip"] ?>:</b></div>
                                <div class="col-md-3"><?= $rs[0]["nrodocumento"] ?></div>
                            </div>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Correo:</b></div>
                                <div class="col-md-3"><?= $rs[0]["correo"] ?></div>
                                <div class="col-md-3 text-right"><b>Teléfono:</b></div>
                                <div class="col-md-3"><?= $rs[0]["telefono"] ?></div>
                            </div>
                            <hr>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Asunto:</b></div>
                                <div class="col-md-9 text-left"><?= stripslashes($rs[0]["asunto"]) ?></div>
                            </div>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Sumilla:</b></div>
                                <div class="col-md-9 text-left"><?= stripslashes($rs[0]["sumilla"]) ?></div>
                            </div>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Visualizar adjunto:</b></div>
                                <div class="col-md-9 text-left"><span class='glyphicon glyphicon-eye-open' aria-hidden='true' data-toggle='tooltip' title='Visualizar Trámite' onclick="window.open('<?= $ruta; ?>');"></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-4 text-right"><button class="btn btn-success btn-lg" type="button" onclick="aprobar('<?= $rs[0]['idTramite']; ?>');">APROBAR</button></div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-left"><button class="btn btn-warning btn-lg" type="button" onclick="rechazar('<?= $rs[0]['idTramite']; ?>');">RECHAZAR</button></div>
                    </div>
                </form>
            </div>
        </div>
    <?php
    }

    function mostrar_rechazar_tramite($idTramite)
    {
    ?>
        <script language='JavaScript'>
            $(document).ready(function() {
                $("#formulario-notificar").submit(function(event) {
                    var dataString = $("#formulario-notificar").serialize();



                    $.ajax({
                        type: "POST",
                        headers: {
                            'Authorization': 'token 3dae27e1b9702aefa8341474b9e422cc8ca2cad6',
                            'Content-Type': 'application/json',

                        },

                        url: "https://api.matricula.edu.pe/api/send-email",

                        data: JSON.stringify({
                            para: $('#cliente_correo').val(),
                            copia: 'alazo@dirislimasur.gob.pe',
                            mensaje: $('#observaciones').val(),
                            cliente_nombre: $("#cliente_nombre").val()
                        }),

                        beforeSend: function() {},
                        success: function(data) {}
                    });




                    $.ajax({
                        type: "POST",
                        url: "../Controlador/C_Tramite.php",
                        data: dataString,
                        beforeSend: function() {
                            $('#guardar').prop('disabled', true);
                            $('#rechazo').modal("hide");
                            $('#barra').modal("show");
                        },
                        success: function(data) {
                            $('#barra').modal("hide");
                            $('#guardar').prop('disabled', false);
                            $('#modal_evaluar').modal("hide");

                            window.location.replace("../Vista/lista_tramite.php");
                            //$('#sep').html(data);
                        }
                    });
                    event.preventDefault();
                });
            });
        </script>
        <style>
            .separar {
                margin: 20px;
            }

            .separar2 {
                margin-top: 45px;
                margin-bottom: 30px;
            }
        </style>
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title text-center"><b>Rechazar Trámite</b></h2>
                </div>
                <form id="formulario-notificar" name="formulario-notificar" autocomplete="off">
                    <input type="hidden" name="idTramite" id="idTramite" value="<?= $idTramite; ?>">
                    <input type="hidden" name="opcion" id="opcion" value="5">
                    <div class="modal-body">
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Motivos:</b></div>
                            <div class="col-md-9 text-left"><textarea name="observaciones" id="observaciones" cols="50%" rows="4" style="text-transform:uppercase;"></textarea></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-center">
                            <button class="btn btn-primary" id="guardar" type="submit">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php
    }

    function guardar_rechazo_tramite($idTramite, $observaciones)
    {
        $upd = "UPDATE tramite_aprobar SET observaciones = '$observaciones', estado = '3', fechaEvaluacion = GETDATE(), idUsuario = '$_SESSION[idUsuario]' WHERE idTramite = '$idTramite'";
        $this->objDatos->ejecutar($upd); //echo $upd;die();

        //notificacion de correo
        $sql = "SELECT correo FROM tramite_aprobar WHERE idTramite = '$idTramite'";
        $rs = $this->objDatos->listar($sql);
        $destino = $rs[0]["correo"];

        //NOTIFICACION POR CORREO
        $asunto = "NOTIFICACIÓN DE TRÁMITE VIRTUAL - DIRIS LIMA SUR";
        $mensaje = "<html><body>";
        $mensaje .= "<table width='100%' style='border:1px solid #0C6;'>";
        $mensaje .= "<tr><td style='font-size:14px; font-family: Arial;' align='left'>Su trámite ha sido rechazado por el siguiente motivo:<br />'" . $observaciones . "'</td></tr>";
        $mensaje .= "</table>";
        $mensaje .= "</body></html>";

        // configuramos la cabecera que llevara el correo


        $cabeceras = 'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
            'From: prensadls@dirisls.gob.pe' . "\r\n" .
            'Reply-To: no-reply@dirislc.gob.pe' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();


        @mail($destino, $asunto, $mensaje, $cabeceras);
        //fin notificacion
    }

    function mostrar_aprobar_tramite($idTramite)
    {
        $sql = "SELECT correo, telefono FROM tramite_aprobar WHERE idTramite='$idTramite'";
        $rs = $this->objDatos->listar($sql); //echo $sql;
    ?>
        <script language='JavaScript'>
            $(document).ready(function() {
                $("#guardar").prop('disabled', true);

                $("#formulario-aprobar").submit(function(event) {
                    var dataString = $("#formulario-aprobar").serialize();



                    $.ajax({
                        type: "POST",
                        headers: {
                            'Authorization': 'token 3dae27e1b9702aefa8341474b9e422cc8ca2cad6',
                            'Content-Type': 'application/json',
                        },

                        url: "https://api.matricula.edu.pe/api/send-email",

                        data: JSON.stringify({
                            para: $('#correo_main').val(),
                            copia: 'alazo@dirislimasur.gob.pe',
                            mensaje: $('#mensaje').val(),
                            cliente_nombre: $("#cliente_nombre").val()
                        }),

                        beforeSend: function() {},
                        success: function(data) {}
                    });


                    $.ajax({
                        type: "POST",
                        url: "../Controlador/C_Tramite.php",
                        data: dataString,
                        beforeSend: function() {
                            $('#guardar').prop('disabled', true);
                            $('#rechazo').modal("hide");
                            $('#barra').modal("show");
                        },
                        success: function(data) {

                            $('#barra').modal("hide");
                            $('#guardar').prop('disabled', false);
                            $('#modal_evaluar').modal("hide");

                            /* INICIO ENVIAR EMAIL API -----------*/

                            window.location.replace("../Vista/lista_tramite.php");
                            //$('#sep').html(data);
                        }
                    })



                    /* FIN ENVIAR EMAIL API -----------*/

                    event.preventDefault();
                });

                $("#oficina_destino").change(function() {
                    $("#oficina_destino option:selected").each(function() {
                        oficina_destino = $(this).val();
                        if (oficina_destino != "") {
                            $.post("../Controlador/C_Tramite.php", {
                                oficina_destino: oficina_destino,
                                opcion: "7"
                            }, function(data) {
                                $("#guardar").prop('disabled', false);
                                $("#responsable").html(data);
                            });
                        } else {
                            $("#guardar").prop('disabled', true);
                        }
                    });
                });
            });
        </script>
        <style>
            .separar {
                margin: 20px;
            }

            .separar2 {
                margin-top: 45px;
                margin-bottom: 30px;
            }
        </style>
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title text-center"><b>Aprobar Trámite</b></h2>
                </div>
                <form id="formulario-aprobar" name="formulario-aprobar" autocomplete="off">
                    <input type="hidden" name="idTramite" id="idTramite" value="<?= $idTramite; ?>">
                    <input type="hidden" name="opcion" id="opcion" value="8">
                    <div class="modal-body">
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Oficina Destino:</b></div>
                            <div class="col-md-9 text-left">
                                <select class="form-control" id="oficina_destino" name="oficina_destino">
                                    <option value="">Seleccione:</option>
                                    <?php
                                    $sql = "SELECT UorgCCod, UorgTDescrip FROM oeimUnidOrg WHERE UorgCCod IN ('00001', '00002', '00005', '00006', '00007') ORDER BY UorgTDescrip ASC";
                                    $Dep = $this->objDatos->listar($sql);
                                    foreach ($Dep as $dep) :
                                    ?>
                                        <option value="<?= $dep["UorgCCod"] ?>"><?= strtoupper(utf8_encode($dep["UorgTDescrip"])); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Responsable:</b></div>
                            <div class="col-md-9 text-left" id="responsable"></div>
                        </div>

                        <input name="correo_main" value="<?= $rs[0]["correo"] ?>" id="correo_main" hidden>


                        <?php if ($rs[0]["correo"] == "" && $rs[0]["telefono"] != "") { ?>
                            <div class="alert alert-warning" role="alert">El remitente no ha indicado un correo electrónico pero si un teléfono: <b><?= $rs[0]["telefono"] ?></b></div>
                        <?php } elseif ($rs[0]["correo"] != "" && $rs[0]["telefono"] == "") { ?>
                            <div class="alert alert-success" role="alert">El mensaje será enviado al correo: <b><?= $rs[0]["correo"] ?></b></div>
                        <?php } elseif ($rs[0]["correo"] == "" && $rs[0]["telefono"] == "") { ?>
                            <div class="alert alert-danger" role="alert"><b>El remitente no ha indicado un correo electrónico y tampoco un teléfono de contacto</b></div>
                        <?php } elseif ($rs[0]["correo"] != "" && $rs[0]["telefono"] != "") { ?>
                            <div class="alert alert-success" role="alert">El mensaje será enviado al correo: <b><?= $rs[0]["correo"] ?></b></div>
                        <?php } ?>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Mensaje al correo del remitente:</b></div>
                            <div class="col-md-9 text-left">
                                <textarea name="mensaje" id="mensaje" cols="50%" rows="4">Se registró con el número de expediente:</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-center">
                            <button class="btn btn-primary" id="guardar" type="submit">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    <?php
    }

    function mostrar_responsable($oficina_destino)
    {
        $sql = "SELECT UorgEncargado FROM oeimUnidOrg WHERE UorgCCod = '$oficina_destino'";
        $rs = $this->objDatos->listar($sql);

        echo "<b>" . strtoupper($rs[0]["UorgEncargado"]) . "</b>";
    }

    function guardar_respuesta($idTramite, $oficina_destino, $mensaje_res)
    {
        $upd = "UPDATE tramite_aprobar SET oficina_destino = '$oficina_destino', mensaje_respuesta = '$mensaje_res', estado = '2', fechaEvaluacion = GETDATE(), idUsuario = '$_SESSION[idUsuario]' WHERE idTramite = '$idTramite'";
        $this->objDatos->ejecutar($upd); //echo $upd;

        $sql = "SELECT correo FROM tramite_aprobar WHERE idTramite = '$idTramite'";
        $rs = $this->objDatos->listar($sql);
        $destino = $rs[0]["correo"];
        //$destino="yeyson107@gmail.com";

        //NOTIFICACION POR CORREO
        $asunto = "NOTIFICACIÓN DE TRÁMITE VIRTUAL - DIRIS LIMA SUR";
        $mensaje = "<html><body>";
        $mensaje .= "<table width='100%' style='border:1px solid #0C6;'>";
        $mensaje .= "<tr><td style='font-size:14px; font-family: Arial;' align='left'><h3 align='center'>La Dirección de Redes Integradas de Salud Lima Sur, le da la más cordial bienvenida.</h3><br />Su trámite ha sido registrado con éxito.<br /><b>" . $mensaje_res . "</b><br />Puede consultar el estado de su trámite desde el siguiente <a href='http://consultatramite.dirislimasur.gob.pe:8085/consulta/' target='_blank'>enlace</a></td></tr>";
        $mensaje .= "</table>";
        $mensaje .= "</body></html>";

        // configuramos la cabecera que llevara el correo
        $cabeceras = 'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
            'From: prensadls@dirisls.gob.pe' . "\r\n" .
            'Reply-To: no-reply@dirislc.gob.pe' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        @mail($destino, $asunto, $mensaje, $cabeceras);
        echo '
            <script>
                console.log("MENSAJE ENVIADO");
            </script>
        ';
        //FIN NOTIFICACION CORREO
    }

    function mostrar_listado()
    {
        $sql = "SELECT ta.asunto, ta.tipo_persona, ta.entidad, ta.representante, td.sDocuDesc FROM tramite_aprobar ta LEFT JOIN Tipo_Documento td ON ta.tipo_tramite = td.cDocuType ORDER BY ta.idTramite DESC";
        $rs = $this->objDatos->listar($sql);
    ?>
        <script language='JavaScript'>
            $(document).ready(function() {
                var table = $("#tramites").DataTable({
                    "ordering": false,
                    "lengthMenu": [
                        [25, 50, -1],
                        [25, 50, "Todos"]
                    ]
                });
            });

            $('[data-toggle="tooltip"]').tooltip();
        </script>
        <table id="tramites" class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th class="text-center">ENTIDAD / REPRESENTANTE</th>
                    <th class="text-center">TIPO PERSONA</th>
                    <th class="text-center">ASUNTO</th>
                    <th class="text-center">TIPO TRÁMITE</th>
                    <th class="text-center">OPC.</th>
                </tr>
            </thead>

            <tbody>
                <?php foreach ($rs as $Rs) {
                    if ($Rs["tipo_persona"] == "1") {
                        $tipPer = "PERSONA NATURAL";
                    }
                    if ($Rs["tipo_persona"] == "2") {
                        $tipPer = "ENTIDAD PÚBLICA O PRIVADA";
                    }
                ?>
                    <tr>
                        <td class="text-left"><b><?= $Rs["entidad"]; ?></b><br /><small><?= $Rs["representante"]; ?></small></td>
                        <td class="text-center"><?= $tipPer; ?></td>
                        <td class="text-left"><?= $Rs["asunto"]; ?></td>
                        <td class="text-center"><?= $Rs["sDocuDesc"]; ?></td>
                        <td class="text-center">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle='tooltip' title="Evaluar Trámite" onClick="mostrar_editar(<?= $Rs['idPaciente']; ?>);"></span>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php
    }

    function mostrar_detalles($idTramite)
    {
        $sql = "SELECT ta.idTramite, ta.asunto, ta.tipo_persona, ta.entidad, ta.representante, ta.nrodocumento, ta.correo, ta.telefono, ta.sumilla, ta.archivo_entidad, ta.estado, ta.mensaje_respuesta, FORMAT(ta.fechaEvaluacion, 'dd-MM-yyyy') AS Fecha, FORMAT(ta.fechaEvaluacion, 'hh:mm') AS Hora, td.sDocuDesc, oe.TipDocPerTDescrip, uo.UorgTDescrip FROM tramite_aprobar ta 
			LEFT JOIN Tipo_Documento td ON ta.tipo_tramite = td.cDocuType
			LEFT JOIN oeimTipDocPersonal oe ON ta.tipo_documento = oe.TipDocPerCCod
			LEFT JOIN oeimUnidOrg uo ON ta.oficina_destino = uo.UorgCCod
			WHERE ta.idTramite = '$idTramite'";
        $rs = $this->objDatos->listar($sql); //echo $sql;
    ?>
        <style>
            .separar {
                margin: 20px;
            }

            .separar2 {
                margin-top: 45px;
                margin-bottom: 30px;
            }
        </style>
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title text-center"><b>Detalle de Trámite</b></h2>
                </div>
                <div class="modal-body">
                    <div class="tab-content">
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Tipo Trámite:</b></div>
                            <div class="col-md-9 text-left"><?= $rs[0]["sDocuDesc"] ?></div>
                        </div>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Entidad:</b></div>
                            <div class="col-md-9 text-left"><?= $rs[0]["entidad"] ?></div>
                        </div>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Representante:</b></div>
                            <div class="col-md-3"><?= $rs[0]["representante"] ?></div>
                            <div class="col-md-3 text-right"><b><?= $rs[0]["TipDocPerTDescrip"] ?>:</b></div>
                            <div class="col-md-3"><?= $rs[0]["nrodocumento"] ?></div>
                        </div>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Correo:</b></div>
                            <div class="col-md-3"><?= $rs[0]["correo"] ?></div>
                            <div class="col-md-3 text-right"><b>Teléfono:</b></div>
                            <div class="col-md-3"><?= $rs[0]["telefono"] ?></div>
                        </div>
                        <hr>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Asunto:</b></div>
                            <div class="col-md-9 text-left"><?= $rs[0]["asunto"] ?></div>
                        </div>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Sumilla:</b></div>
                            <div class="col-md-9 text-left"><?= $rs[0]["sumilla"] ?></div>
                        </div>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Visualizar adjunto:</b></div>
                            <div class="col-md-9 text-left"><span class='glyphicon glyphicon-eye-open' aria-hidden='true' data-toggle='tooltip' title='Visualizar Trámite' onclick="window.open('<?= $ruta; ?>');"></span></div>
                        </div>
                        <hr>
                        <div class="row separar">
                            <div class="col-md-3 text-right"><b>Estado Trámite:</b></div>
                            <div class="col-md-9 text-left"><b>
                                    <?php
                                    if ($rs[0]["estado"] == "2") {
                                        echo "<span style='color:green'>APROBADO el día " . $rs[0]["Fecha"] . " a las " . $rs[0]["Hora"] . "</span>";
                                    } elseif ($rs[0]["estado"] == "3") {
                                        echo "<span style='color:red'>RECHAZADO el día " . $rs[0]["Fecha"] . " a las " . $rs[0]["Hora"] . "</span>";
                                    }
                                    ?>
                                </b></div>
                        </div>
                        <?php if ($rs[0]["estado"] == "2") { ?>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Oficina Destino:</b></div>
                                <div class="col-md-9 text-left"><b><?= utf8_encode($rs[0]["UorgTDescrip"]) ?></b></div>
                            </div>
                        <?php } ?>
                        <?php if ($rs[0]["estado"] == "3" && $rs[0]["mensaje_respuesta"] != "") { ?>
                            <div class="row separar">
                                <div class="col-md-3 text-right"><b>Motivo:</b></div>
                                <div class="col-md-9 text-left"><b><?= utf8_encode($rs[0]["mensaje_respuesta"]) ?></b></div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-center">
                        <button class="btn btn-danger" type="button" data-dismiss="modal">Cerrar Ventana</button>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
}
?>