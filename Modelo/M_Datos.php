<?php
class M_Datos
{
	private $conexion;
	private $lista;

	public function __construct()
	{
		//conexion local
		/*$servidor = "DESKTOP-USFFK34\SQLEXPRESS";
		$informacion = array("Database" => "TRAMITE_VIRTUAL");*/

		//conexion servidor
		$servidor = "ETFTI09";
		$informacion = array("Database" => "TRAMITE_VIRTUAL", "UID" => "sa", "PWD" => '1597531994', "CharacterSet" => "UTF-8");

		$this->con = sqlsrv_connect($servidor, $informacion);
		date_default_timezone_set('America/Lima');
	}

	//funcion general para buscar
	public function buscar($sql)
	{
		$result = sqlsrv_query($this->con, $sql);
		return $result;
	}

	//funcion general para cerrar la busqueda
	public function cerrarBuscar($datos)
	{
		sqlsrv_free_stmt($datos);
	}

	//funcion general para ejecutar
	public function ejecutar($sql)
	{
		$resultado = sqlsrv_query($this->con, $sql);

		if ($resultado == true) {

			return $rs = "ok";
			
		} else if ($resultado == false) {

			if (($errors = sqlsrv_errors()) != null) {
				foreach ($errors as $error) {
					echo "SQLSTATE: " . $error['SQLSTATE'] . "<br />";
					echo "code: " . $error['code'] . "<br />";
					echo "message: " . $error['message'] . "<br />";
				}
			}
		}
	}

	public function ultimo_id()
	{
		$n_id = $this->conexion->insert_id;
		return $n_id;
	}

	public function execute($sql)
	{
		mysql_query($sql, $this->conectar());
	}

	public function contar($sql)
	{
		$q = sqlsrv_query($this->con, $sql, array(), array("Scrollable" => "buffered"));
		$cnt = sqlsrv_num_rows($q);
		return $cnt;
	}

	//funcion general para cerrar la conexion
	public function cerrarConexion()
	{
		sqlsrv_close($this->con);
	}

	public function listar($sql)
	{
		$this->lista = array();
		$rs = sqlsrv_query($this->con, $sql);
		while ($Rs = sqlsrv_fetch_array($rs)) {
			$this->lista[] = $Rs;
		}
		return $this->lista;
		$this->cerrarBuscar($rs);
		$this->cerrarConexion();
	}
}
